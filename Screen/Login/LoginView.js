import React from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';

export default class LoginView extends React.Component{
    render(){
        return(
            <View style={styles.container}>
                <View>
                    <Text>Logo</Text>
                </View>
                <View>
                    <TextInput placeholder='ID를 입력해주세요' />
                    <TextInput placeholder='PASSWORD를 입력해주세요'/>
                </View>
                <View>
                    <Button title='회원가입' />
                    <Button title='로그인' />
                </View>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
