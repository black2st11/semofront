import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import {RadioButton, Button} from 'react-native-paper'
import axios from 'axios'

export default class RegisterView extends React.Component{
    state ={
        value : true,
        id : '',
        password : '',
        confirmPassword: '',
        email : '',
    }

    handleSubmit=async()=>{
        let data ={
            'username' : this.state.id,
            'email' :  this.state.email,
            'password' : this.state.password
        }
        await axios({
            url: `http://localhost:8000/`,
            method:"POST",
            data : data,
            headers : {
                'Content-Type' : 'application/json'
            }
        })
    }

    render(){

        return(
            <View style={styles.container}>
                <TextInput 
                placeholder="닉네임" 
                value={this.state.id} 
                onChangeText={e=>this.setState({id:e})}
                />
                <TextInput 
                placeholder="email" 
                value={this.state.email} 
                onChangeText={e=>this.setState({email:e})}
                />
                <TextInput 
                placeholder='password' 
                value={this.state.password} 
                onChangeText={e=>this.setState({password:e})}
                />
                <TextInput 
                placeholder='비밀번호 확인' 
                value={this.state.confirmPassword} 
                onChangeText={e=>this.setState({confirmPassword:e})}
                />
                <RadioButton.Group
                    onValueChange={value => this.setState({ value })}
                    value={!this.state.value}>
                    <View>
                        <Text>동의</Text>
                        <RadioButton value={this.state.value?false:true} />
                    </View>
                </RadioButton.Group>
                <Button mode='contained' icon="" onPress={this.handleSubmit}>Press me</Button>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });