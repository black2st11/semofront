import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import LoginView from './Screen/Login/LoginView'
import RegisterView from './Screen/Register/RegisterView'
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'

const Stack = createStackNavigator();

export default class App extends React.Component{
  render(){
    return (
      <RegisterView />
    )
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
